package controller;

import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.UserDao;
import model.User;
import util.PasswordEncorder;


/**
 * Servlet implementation class UserAddServlet
 */
@WebServlet("/UserAddServlet")
public class UserAddServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserAddServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

      HttpSession session = request.getSession();
      if (session.getAttribute("userInfo") == null) {
        response.sendRedirect("LoginServlet");
        return;
      }

      String loginId = request.getParameter("login-id");
      request.setAttribute("loginId", loginId);
      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userAdd.jsp");
      dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      request.setCharacterEncoding("UTF-8");
      String inputLoginId = request.getParameter("user-loginid");
      String password = request.getParameter("password");
      String name = request.getParameter("user-name");
      String birthDate = request.getParameter("birth-date");
      String confirmPassword = request.getParameter("password-confirm");
      

      UserDao userDao = new UserDao();
      List<User> userList = userDao.findAll();
      boolean sameLoginId = false;
      for (User user : userList) {
        if (inputLoginId.equals(user.getLoginId())) {
          sameLoginId = true;
        }
      }



      if (inputLoginId.equals("") || password.equals("") || name.equals("") || birthDate.equals("")
          || confirmPassword.equals("") || !(password.equals(confirmPassword)) || sameLoginId) {
        request.setAttribute("errMsg", "入力された内容は正しくありません");
        request.setAttribute("inputLoginId", inputLoginId);
        request.setAttribute("name", name);
        request.setAttribute("birthDate", birthDate);
        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userAdd.jsp");
        dispatcher.forward(request, response);
        return;
      }


      password = PasswordEncorder.encordPassword(password);

      userDao.addUser(inputLoginId, password, name, birthDate);
      response.sendRedirect("UserListServlet");

	}

}
