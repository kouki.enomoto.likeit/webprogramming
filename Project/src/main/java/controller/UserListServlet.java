package controller;

import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserListServlet
 */
@WebServlet("/UserListServlet")
public class UserListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserListServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	  
      HttpSession session = request.getSession();
      User loginUser = (User) session.getAttribute("userInfo");

      if (loginUser == null) {
        response.sendRedirect("LoginServlet");
        return;

      }

      // ログインセッションがない場合ログイン画面にリダイレクト
      UserDao userDao = new UserDao();
      List<User> userList = userDao.findAll();

      request.setAttribute("userList", userList);

      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userList.jsp");
      dispatcher.forward(request, response);
      return;
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      request.setCharacterEncoding("UTF-8");
      String loginId = request.getParameter("user-loginid");
      String userName = request.getParameter("user-name");
      String birthDate = request.getParameter("date-start");
      String dateEnd = request.getParameter("date-end");
      request.setAttribute("loginId", loginId);
      request.setAttribute("name", userName);
      request.setAttribute("dateStart", birthDate);
      request.setAttribute("dateEnd", dateEnd);


      UserDao userDao = new UserDao();
      List<User> userList = userDao.serch(loginId, userName, birthDate, dateEnd);
      request.setAttribute("userList", userList);



      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userList.jsp");
      dispatcher.forward(request, response);
      return;

	}

}
