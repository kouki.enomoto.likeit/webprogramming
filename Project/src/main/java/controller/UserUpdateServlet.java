package controller;

import java.io.IOException;
import java.sql.Date;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.UserDao;
import model.User;
import util.PasswordEncorder;

/**
 * Servlet implementation class userUpdateServlet
 */
@WebServlet("/UserUpdateServlet")
public class UserUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserUpdateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

      HttpSession session = request.getSession();
      User userInfo = (User) session.getAttribute("userInfo");
      if (userInfo == null) {
        response.sendRedirect("LoginServlet");
        return;
      }

      String id = request.getParameter("id");
      int userId = Integer.valueOf(id);
      UserDao userDao = new UserDao();
      User user = userDao.findById(userId);
      request.setAttribute("userUpdate", user);

      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
      dispatcher.forward(request, response);


	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      request.setCharacterEncoding("UTF-8");
      String id = request.getParameter("user-id");
      int userId = Integer.valueOf(id);
      String loginId = request.getParameter("login-id");
      String password = request.getParameter("password");
      String confirmPassword = request.getParameter("password-confirm");
      String name = request.getParameter("user-name");
      String birthDate = request.getParameter("birth-date");
      
      if (birthDate.equals("")) {
        User user = new User(userId, loginId, name, null, password, false, null, null);
        request.setAttribute("userUpdate", user);
        request.setAttribute("errMsg", "入力された内容は正しくありません");


        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
        dispatcher.forward(request, response);
        return;
      }


      else if (name.equals("") || !(password.equals(confirmPassword))) {
        Date birth = Date.valueOf(birthDate);

        User user = new User(userId, loginId, name, birth, password, false, null, null);
          request.setAttribute("userUpdate", user);

          request.setAttribute("errMsg", "入力された内容は正しくありません");
          

          RequestDispatcher dispatcher =
              request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
          dispatcher.forward(request, response);
          return;

      }

      password = PasswordEncorder.encordPassword(password);
      UserDao userDao = new UserDao();
      userDao.updateUser(userId, password, name, birthDate);

      response.sendRedirect("UserListServlet");
	}

}
