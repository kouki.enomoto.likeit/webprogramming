package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import model.User;

public class UserDao {

  public User findByLoginInfo(String loginId, String password) {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";

      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, loginId);
      pStmt.setString(2, password);
      ResultSet rs = pStmt.executeQuery();

      if (!rs.next()) {
        return null;
      }

      int id = rs.getInt("id");
      String _loginId = rs.getString("login_id"); // 変数名がメソッドの引数と同じになってしまうため、別の名前(_loginId)を設定する
      String name = rs.getString("name");
      Date birthDate = rs.getDate("birth_date");
      String _password = rs.getString("password"); // 変数名がメソッドの引数と同じになってしまうため、別の名前を設定する
      boolean isAdmin = rs.getBoolean("is_admin");
      Timestamp createDate = rs.getTimestamp("create_date");
      Timestamp updateDate = rs.getTimestamp("update_date");
      return new User(id, _loginId, name, birthDate, _password, isAdmin, createDate, updateDate);


    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }


  }

  // findAll、メソッド
  public List<User> findAll(){
    Connection conn = null;
    List<User> userList = new ArrayList<User>();

    try {

      conn = DBManager.getConnection();
      String sql = "SELECT * FROM user WHERE is_admin = false";

      Statement stmt = conn.createStatement();
      ResultSet rs = stmt.executeQuery(sql);

      while (rs.next()) {
        int id = rs.getInt("id");
        String loginId = rs.getString("login_id");
        String name = rs.getString("name");
        Date birthDate = rs.getDate("birth_date");
        String password = rs.getString("password");
        boolean isAdmin = rs.getBoolean("is_admin");
        Timestamp createDate = rs.getTimestamp("create_date");
        Timestamp updateDate = rs.getTimestamp("update_date");
        User user =
            new User(id, loginId, name, birthDate, password, isAdmin, createDate, updateDate);

        userList.add(user);

      }


    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
    return userList;
  }

  public void addUser(String loginId, String password, String name, String birthDate) {
    Connection conn = null;
    try {

      conn = DBManager.getConnection();
      String sql ="INSERT INTO user(login_id, name, birth_date, password, create_date, update_date)VALUES(?,?,?,?,now(),now())";
      
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, loginId);
      pStmt.setString(2, name);
      pStmt.setString(3, birthDate);
      pStmt.setString(4, password);
      int result = pStmt.executeUpdate();



      // TODO ここに処理を書いていく

    } catch (SQLException e) {
      e.printStackTrace();
    } finally {

      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }

  }

  public User findById(int userId) {

    Connection conn = null;
    try {

      conn = DBManager.getConnection();
      String sql = "SELECT * FROM user WHERE id =?";

      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setInt(1, userId);
      ResultSet rs = pStmt.executeQuery();

      if (!rs.next()) {
        return null;
      }

      int id = rs.getInt("id");
      String _loginId = rs.getString("login_id");
      String name = rs.getString("name");
      Date birthDate = rs.getDate("birth_date");
      String _password = rs.getString("password");
      boolean isAdmin = rs.getBoolean("is_admin");
      Timestamp createDate = rs.getTimestamp("create_date");
      Timestamp updateDate = rs.getTimestamp("update_date");
      return new User(id, _loginId, name, birthDate, _password, isAdmin, createDate, updateDate);



    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {

      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }

    

  }

  public void updateUser(int id, String password, String name, String birthDate) {
    

    Connection conn = null;
    try {
      conn = DBManager.getConnection();
      String sql =
          "UPDATE user SET name =?, password =?, birth_date =?, update_date =NOW() WHERE id =?";
      
      PreparedStatement pStmt =conn.prepareStatement(sql);
      pStmt.setString(1,name);
      pStmt.setString(2, password);
      pStmt.setString(3, birthDate);
      pStmt.setInt(4, id);
      int result = pStmt.executeUpdate();
      
      
      
    } catch (SQLException e) {
      e.printStackTrace();
    } finally {

      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }

  }


  public void deleteUser(int id) {

    Connection conn = null;
    try {

      conn = DBManager.getConnection();
      String sql = "DELETE FROM user WHERE id =?";

      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setInt(1, id);
      int result = pStmt.executeUpdate();

    } catch (SQLException e) {
      e.printStackTrace();
    } finally {

      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }



  }

  public List<User> serch(String userId, String userName, String birthDate, String endBirthDate) {
    Connection conn = null;
    List<User> userList = new ArrayList<User>();

    try {

      conn = DBManager.getConnection();
      String sql =
          "SELECT * FROM user WHERE is_admin = false";


      StringBuilder stringBuilder = new StringBuilder(sql);
      List<String> list = new ArrayList<String>();

      if (!userId.equals("")) {
        stringBuilder.append(" AND login_id =?");
        list.add(userId);
      }

      if (!userName.equals("")) {
        stringBuilder.append(" AND name LIKE ?");
        list.add("%" + userName + "%");
      }
      if (!birthDate.equals("")) {
        stringBuilder.append(" AND birth_date >= ?");
        list.add(birthDate);
      }
      if (!endBirthDate.equals("")) {
        stringBuilder.append(" AND birth_date <= ?");
        list.add(endBirthDate);
      }

      PreparedStatement pStmt = conn.prepareStatement(stringBuilder.toString());
      for (int i = 0; i < list.size(); i++) {
        pStmt.setString(i + 1, list.get(i));
      }

      ResultSet rs = pStmt.executeQuery();

      while (rs.next()) {
        int id = rs.getInt("id");
        String loginId = rs.getString("login_id");
        String name = rs.getString("name");
        Date _birthDate = rs.getDate("birth_date");
        String password = rs.getString("password");
        boolean isAdmin = rs.getBoolean("is_admin");
        Timestamp createDate = rs.getTimestamp("create_date");
        Timestamp updateDate = rs.getTimestamp("update_date");
        User user =
            new User(id, loginId, name, _birthDate, password, isAdmin, createDate, updateDate);

        userList.add(user);

      }


    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
    return userList;
  }

}
