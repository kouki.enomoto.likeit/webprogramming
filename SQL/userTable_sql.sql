﻿
CREATE DATABASE usermanagement default character set utf8;
USE usermanagement;

CREATE TABLE user(
id serial PRIMARY KEY UNIQUE NOT NULL AUTO_INCREMENT,
login_id VARCHAR(255) UNIQUE NOT NULL,
name VARCHAR(255) NOT NULL,
birth_date DATE NOT NULL,
password VARCHAR(255) NOT NULL,
is_admin BOOLEAN NOT NULL default 0,
create_date DATETIME NOT NULL,
update_date DATETIME NOT NULL
);



INSERT INTO user(login_id, name, birth_date, password, is_admin, create_date, update_date) values(
'admin',
'管理者',
'1997-07-04',
'password',
1,
NOW(),
NOW()
);


COMMENT ON COLUMN user.id IS 'ID';
COMMENT ON COLUMN user.login_id IS 'ログインID';
COMMENT ON COLUMN user.name IS '名前';
COMMENT ON COLUMN user.birth_date IS '生年月日';
COMMENT ON COLUMN user.password IS 'パスワード';
COMMENT ON COLUMN user.is_admin IS '管理者フラグ';
COMMENT ON COLUMN user.create_date IS '作成日時';
COMMENT ON COLUMN user.update_date IS '更新日時';

